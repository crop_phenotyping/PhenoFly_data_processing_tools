
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
from pathlib import Path
import imageio
from scipy import ndimage as ndi
from skimage.feature import peak_local_max
from scipy.ndimage.measurements import center_of_mass, label
from skimage.morphology import watershed
from scipy.sparse import csr_matrix
import math

from matplotlib import pyplot as plt
from skimage.morphology import disk
from skimage.filters import rank

from sklearn import svm
from sklearn.preprocessing import StandardScaler

# Helper functions

def tiller_prediction(LA, GDD_BBCH30):
    """
    Predicts tiller count pased on multi-view leaf area
    :param LA: Multi-view leaf area per m^2
    :param GDD: Growing degree days since sowing
    :return: Estimation of tiller count
    """

    # LA is m^2/m^2 (= mm^2/m^2)
    LA = LA * 10**6
    # scale to plot size of 125 * 50 * 3 (mm)
    LA = (LA / 1.0) * (0.125 * 0.150)

    #Asym: Model was fit to plot size of 125 * 50 * 3 (mm), 90% canopy coverage as theoretical maximum
    Asym = ((125 * 50 * 3) * 0.9 )

    # Prevent complex numbers if LA is larger than expected Asymp
    if LA > Asym:
        LA = Asym

    # xmid and scal: Dependend on GDD_BBCH30, empirical estimation with 2018/2019 data
    xmid = 3.210957888 + GDD_BBCH30 * -0.004929346
    scal = 0.806696378 + GDD_BBCH30 * 0.002618851

    # Dependence leaf area to tiller count:
    # LA ~ SSlogis(log(tiller_count+1), 125 * 50 * 3, xmid, scal)

    # Solving for tiller_count:

    # LA ~ Asym / (1 + exp((xmid - log(x+1) )/scal))
    # y ~  a    / (1 + exp((m -    log(x+1) )/s))
    # Wolframalpha tells me:
    # solve(y =  a    / (1 + exp((m -    log(x+1) )/s)), x)
    # x = (a/y - 1)^(-s) (e^m - (a/y - 1)^s)
    # Therefore:
    tiller_count = (Asym / LA - 1)**(-scal) * (math.exp(xmid) - (Asym / LA - 1) ** scal)
    tiller_count = tiller_count if tiller_count > 0 else 0
    
    # Scale count to 1 m^2
    tiller_count = (tiller_count / (0.125 * 0.15) ) * 1.0

    return(tiller_count)


def plant_prediction(image, delta_to_BBCH30, GSD):
    """
    Extracts plant regions (number and size) using local maxima and watershed
    :param image: Multi-view leaf area image from plot in BBCH31 / ~ GDD 470-490 stage
    :return: Total plant count, individual sizes of regions
    """

    if delta_to_BBCH30 == 15:
        w_intercept = 1.4
        w_slope = 0.000855
        gc_intercept = 4.2
        gc_slope = 0.000547


    elif delta_to_BBCH30 == 10:
        w_intercept = 0.99
        w_slope = 0.000755
        gc_intercept = 3.8
        gc_slope = 0.000473

    elif delta_to_BBCH30 == 5:
        w_intercept = 0.18
        w_slope = 0.000471
        gc_intercept = 2
        gc_slope = 0.000352
    else:
        print("Delta to BBCH30", delta_to_BBCH30, " not implemented, must be in [5, 10, 15]")


    # Minimal distance between two adjacent local maxima
    min_distance_local_peaks = 15
    # Minimum intensity of pixel to become considered as local maxima
    min_intensity_local_peaks = 0.1
    # Minimum intensity of pixel to count as part of watershed area
    min_intensity_watershed_area = 0.50

    # Smooth with mean
    selem = disk(min_distance_local_peaks)
    image_smoothed = rank.mean(image, selem=selem)

    # Calc local maxima
    local_maxima = peak_local_max(image_smoothed, min_distance=min_distance_local_peaks, indices=False,
                                  threshold_abs=int(min_intensity_local_peaks*255), exclude_border=int(min_distance_local_peaks))

    local_maxima_labels, n_max = label(local_maxima)
    # Merge adjacent local maxima
    local_maxima_merged = np.round(center_of_mass( local_maxima, local_maxima_labels, range(1, n_max+1) ))

    if len(local_maxima_merged)>0:

        # Extract watershed regions
        # Create numbered labels for local maxima
        local_maxima_labels = csr_matrix((np.arange(local_maxima_merged.shape[0])+1, (local_maxima_merged[:, 0], local_maxima_merged[:, 1])),
                                         shape=image.shape).toarray()

        # Do watershed with min intensity limit
        watershed_labels = watershed(-image_smoothed, local_maxima_labels, mask=image>min_intensity_watershed_area*255)

        # Summarize area per watershed label
        area_watershed_labels, area_watershed_label_counts = np.unique(watershed_labels, return_counts=True)

        # Remove size of zone "0" (ground)
        area_watershed_label_counts = area_watershed_label_counts[1:]

    else:
        area_watershed_label_counts = []

    a_area_watershed_label_counts = np.array(area_watershed_label_counts)

    w_plant_counts = np.round(a_area_watershed_label_counts * w_slope + w_intercept)
    w_plant_count_total = np.sum(w_plant_counts)

    # Extract ground coverage > 50, normalize to 3 microplots (divide by sampling area, multiply by 3-microplot-area)
    a_area_gc50 = ( np.sum(image>255 * 0.5) ) / (image.shape[0] * image.shape[1] * (GSD*1000)**2) * (150 * 125)
    # Calculate plant counts
    gc_plant_counts_total = a_area_gc50 * gc_slope + gc_intercept
    gc_plant_counts_total = gc_plant_counts_total / (150 * 125) * (image.shape[0] * image.shape[1] * (GSD*1000)**2)

    return(w_plant_count_total, gc_plant_counts_total)


def process_plant_count(path_campaign, delta_to_BBCH30,
                        GSD):
    """
    Process folder to perform plant count estimation
    :param path_campaign: path of campaigns
    :param campaign_date: Date of campaign
    """

    print("Process campaign ", path_campaign)

    # Container for results
    plant_counts = {}

    campaign_date = path_campaign.parts[-2]

    path_GC_AC_folder = path_campaign / 'GC_AC'

    LA_images = sorted(path_GC_AC_folder.glob('*_?????????*.tif'))

    for LA_image in LA_images:
        parts = LA_image.name.split("_")
        if (len(parts) == 2):
            design_label = parts[0]
            plot_label = parts[1][:-4]
        elif (len(parts) == 3):
            if parts[1] == "lp01":
                print(LA_image, " is lp01 overview image, skip")
                continue
            design_label = parts[0] + "_" + parts[1]
            plot_label = parts[2][:-4]
        elif (len(parts) == 4):
            design_label = parts[0] + "_" + parts[1]
            plot_label = parts[2] + "_" + parts[3][:-4]
        else:
            raise Exception("Do not know how to parse name " + LA_image.name + " with n parts: " + str(len(parts)))

        print("Process LA image for plot", plot_label)

        LA_img = imageio.imread(LA_image)

        w_counts, gc_counts = plant_prediction(LA_img, delta_to_BBCH30, GSD)

        # plant count estimation is absolut number, divide by sampling area size in mm^2
        w_counts_scaled = round(w_counts  / (LA_img.shape[0] * LA_img.shape[1] * (GSD)**2))
        gc_counts_scaled = round(gc_counts / (LA_img.shape[0] * LA_img.shape[1] * (GSD) ** 2))

        print("Plants: w:", w_counts_scaled, "gc: ", gc_counts_scaled, "per m²")

        plant_counts_ = {}
        plant_counts_['plot_label'] = plot_label
        plant_counts_['campaign_date'] = campaign_date
        plant_counts_['watershed_plant_count_estimation'] = w_counts_scaled
        plant_counts_['watershed_plant_count_estimation_abs'] = w_counts
        plant_counts_['groundcoverage_plant_count_estimation'] = gc_counts_scaled
        plant_counts_['groundcoverage_plant_count_estimation_abs'] = gc_counts
        plant_counts_['delta'] = delta_to_BBCH30

        if design_label not in plant_counts:
            plant_counts[design_label] = []

        plant_counts[design_label].append(plant_counts_)

    path_trait_csvs = path_campaign / 'trait_csvs'
    path_trait_csvs.mkdir(exist_ok=True)

    for design_label, plant_region_data_ in plant_counts.items():
        print("Write plant region trait csv for", design_label)

        df_regions_w = pd.DataFrame(plant_region_data_)
        df_regions_gc = pd.DataFrame(plant_region_data_)

        # watershed data
        df_regions_w['trait'] = "PntDen"
        df_regions_w['trait_id'] = 1
        df_regions_w['value'] = df_regions_w['watershed_plant_count_estimation']

        df_regions_w['timestamp'] = pd.to_datetime(df_regions_w['campaign_date'], format="%Y%m%d")

        df_regions_w.to_csv(path_trait_csvs / (design_label + "_watershed_plants.csv"), index=False)

        # GC data
        df_regions_gc['trait'] = "PntDen"
        df_regions_gc['trait_id'] = 1
        df_regions_gc['value'] = df_regions_gc['groundcoverage_plant_count_estimation']

        df_regions_gc['timestamp'] = pd.to_datetime(df_regions_gc['campaign_date'], format="%Y%m%d")

        df_regions_gc.to_csv(path_trait_csvs / (design_label + "_gc_plants.csv"), index=False)



def  process_tiller_count(path_campaign, path_BBCH30_estimation=None, design=None):
    GDD_from = - 200
    GDD_to = 0

    #print("Process campaigns ", path_campaign)

    # Read LA
    path_trait_csvs = path_campaign / 'trait_csvs'
    LA_files = sorted(path_trait_csvs.glob("*_LA.csv"))

    if len(LA_files) > 0:
        
        campaign_date = path_campaign.parts[-2]
    
        campaign_date_long = campaign_date[0:4] + "-" + campaign_date[4:6] + "-" + campaign_date[6:8]
    
        # Read GDD
        path_GDD_csv = path_campaign.parent.parent / 'covariates' / 'GDD.csv'
        df_GDDs = pd.read_csv(path_GDD_csv)
        GDD = df_GDDs.loc[df_GDDs.campaign_date == campaign_date_long, 'GDD'].values[0]
    
        # Read BBCH30 estimation
        df_BBCH30 = pd.read_csv(path_BBCH30_estimation)
        df_BBCH30['delta_GDD_BBCH30'] = GDD - df_BBCH30.value

        # Read LA
        LA_data = {}
        for LA_file in LA_files:
            df_LA = pd.read_csv(LA_file)

            df_LA = pd.merge(df_LA, df_BBCH30[['plot.UID', 'delta_GDD_BBCH30']], left_on="plot_label", right_on='plot.UID')
            #df_LA.drop(columns=['plot.UID'], inplace=True)

            parts = LA_file.name.split("_")
            if (len(parts) == 3):
                if parts[1] == "lp01":
                    print(LA_file, " is lp01 overview image, skip")
                    continue
                design_label = parts[0] + "_" + parts[1]
                plot_label = parts[2][:-4]
            elif (len(parts) == 2):
                design_label = parts[0]
                plot_label = parts[1][:-4]
            elif (len(parts) == 4):
                design_label = parts[0] + "_" + parts[1]
                plot_label = parts[2] + "_" + parts[3][:-4]
            else:
                raise Exception("Do not know how to parse name " + LA_file.name)

            df_LA['LA'] = df_LA['value']

            LA_data[design_label] = df_LA
            
        # Calc tiller count
        for design_label, df_ in LA_data.items():
            if design is not None and design_label != design:
                print("Skip design", design_label)
 
                continue

            print("-----------------------\nGenerate tiller trait csv for", design_label, "for date", campaign_date)
            
            # Filter out too far GDD_BBCH30 values
            df__ = df_.loc[(df_['delta_GDD_BBCH30'] > GDD_from) & (df_['delta_GDD_BBCH30'] < GDD_to)].copy()

            df__['tiller_estimation'] = [
                tiller_prediction(row['LA'], row['delta_GDD_BBCH30']) for index, row in df__.iterrows()]

            df__['trait'] = "PltTilDen"
            df__['trait_id'] = 34

            df__['timestamp'] = datetime.strptime(campaign_date, "%Y%m%d")

            df__ = df__[['trait', 'trait_id', 'tiller_estimation', 'timestamp', 'plot_label', 'delta_GDD_BBCH30', 'LA']].copy()

            df__['value'] = df__['tiller_estimation']
            df__['value_json'] = df__.apply(lambda row: row.iloc[4:6].to_json(), axis=1)
            
            path_trait_file = path_trait_csvs / (design_label + "_bbch30estimation-tillers.csv")
            if len(df__) > 0:
                print("Calcualted tillers, bbch30 estimation: median",
                      np.nanmedian(df__['value']))
                print("Median GDD_BBCH30 measured:", df__['delta_GDD_BBCH30'].median())
                print("Median LA:", df__['LA'].median())
                df__.to_csv(path_trait_file, index=False)
            else:
                print("No BBCH30 estimation left after filtering, delete potential trait file")
                try:
                    path_trait_file.unlink()
                except FileNotFoundError:
                    print("No file")
    else:
        print("-------------\nNo LA data found, skip campaign", str(path_campaign))



def process_NadirCC_LCCC_LA_BBCH30(path_campaign, GSD):

    campaign_date = path_campaign.parts[-2]

    # Trains SVM
    df_training = pd.read_csv('./EarlyGrowthTraitExtraction/svm_SE_training.csv')
    features_col = [col for col in df_training if col.startswith('gt_bin')]

    sc_x = StandardScaler()

    X = df_training[features_col]
    sc_x.fit(X)
    X = sc_x.transform(X)

    # Log delta to weight close-to-zero values higher
    #y = np.log1p(np.abs(df_training['delta_to_BBCH'])) * np.sign(df_training['delta_to_BBCH'])
    y = df_training['delta_to_BBCH30_GDD']

    svm_predictor = svm.SVR(C=32, kernel='rbf', gamma=0.125, epsilon=0.1)
    
    svm_predictor.fit(X, y)
    print("SVM initialized, support vectors:", len(svm_predictor.support_vectors_))

    print("Process campaign ", path_campaign)

    # Read / calc traits

    path_GC_AC_folder = path_campaign / 'GC_AC'
    path_trait_csvs = path_campaign / 'trait_csvs'

    ## CC based on nadir image
    CC_files = sorted(path_GC_AC_folder.glob('*_canopy_coverages.csv'))
    for CC_file in CC_files:
        design_label = CC_file.name[:-21]
        print("Process CC nadir file for design", design_label)

        df_CC = pd.read_csv(CC_file)
        df_CC_soil = df_CC.loc[df_CC.type == 'soil']
        # Select max zenith angle (in radian) -> most nadir view
        df_CC_nadir = df_CC_soil.loc[df_CC_soil.groupby('plot_label').zenith_angle.idxmax()]

        df_CC_nadir['value_json'] = df_CC_nadir.apply(lambda row: row.iloc[1:].to_json(), axis=1)

        df_CC_nadir['trait'] = "CnpCov"
        df_CC_nadir['trait_id'] = 38
        df_CC_nadir['value'] = df_CC_nadir['canopy_coverage']

        df_CC_nadir['timestamp'] =  datetime.strptime(campaign_date, "%Y%m%d")

        df_CC_nadir.to_csv(path_trait_csvs / (design_label + "_CC_nadir.csv"), index=False)

    # CC based on LC/CC model
    CC_LC_files = sorted(path_GC_AC_folder.glob('*_CC_LC.csv'))

    for CC_LC_file in CC_LC_files:
        design_label = CC_LC_file.name[:-10]
        print("Process CC LC file for design", design_label)

        # Read file in LC_CC folder and convert to trait file
        df_CC_LC = pd.read_csv(CC_LC_file)
        df_CC_LC['value_json'] = df_CC_LC.apply(lambda row: row.iloc[1:].to_json(), axis=1)

        df_CC_LC['trait'] = "CnpCov"
        df_CC_LC['trait_id'] = 38
        df_CC_LC['value'] = df_CC_LC['CC']

        df_CC_LC['timestamp'] = datetime.strptime(campaign_date, "%Y%m%d")

        df_CC_LC.to_csv(path_trait_csvs / (design_label + "_CC_LC.csv"), index=False)

    # LA
    # Container for results
    LA_data = {}
    plant_region_data = {}

    LA_images = sorted(path_GC_AC_folder.glob('*_?????????*.tif'))

    for LA_image in LA_images:
        parts = LA_image.name.split("_")
        if(len(parts)==3):
            if parts[1] == "lp01":
                print(LA_image, " is lp01 overview image, skip")
                continue
            design_label = parts[0] + "_" + parts[1]
            plot_label = parts[2][:-4]
        elif(len(parts)==2):
            design_label = parts[0]
            plot_label = parts[1][:-4]
        elif (len(parts) == 4):
            design_label = parts[0] + "_" + parts[1]
            plot_label = parts[2] + "_" + parts[3][:-4]
        else:
            raise Exception("Do not know how to parse name " + LA_image.name)


        print("Process LA image for plot", plot_label)

        LA_img = imageio.imread(LA_image)

        # Convert to fraction
        LA_img = LA_img / 255.0
        # Square
        LA_img_powered = np.power(LA_img, 2)

        '''
        gt_bins = {}
        for gt_bin in np.arange(start=0, stop=100, step=1):
            gt_bins[gt_bin] = np.sum(LA_img > gt_bin/100) / (LA_img.shape[0] * LA_img.shape[1])
        '''
        
        gt_bins = {gt_bin : np.sum(LA_img > gt_bin/100) / (LA_img.shape[0] * LA_img.shape[1]) for gt_bin in np.arange(start=0, stop=100, step=1)}
        gt_bins_10 = {gt_bin: np.sum(LA_img > gt_bin / 100) / (LA_img.shape[0] * LA_img.shape[1]) for gt_bin in
                   np.arange(start=0, stop=100, step=10)}

        # normalize
        gt_bins_norm = {}
        for gt_bin in np.arange(start=0, stop=100, step=10):
            sum_a = np.sum([val for val in gt_bins_10.values()])
            if sum_a != 0:
                gt_bins_norm[gt_bin] = gt_bins_10[gt_bin] / sum_a
            else:
                gt_bins_norm[gt_bin] = 0
        # check normalization:
        #print("GT bins normalized to sum: ", np.sum([val for val in gt_bins_norm.values()]))

        # Calc LA and aerial index
        LA = np.sum(LA_img_powered)

        LA_data_ = {}
        LA_data_['plot_label'] = plot_label
        LA_data_['campaign_date'] = campaign_date

        # LA is absolut, divide by sampling area size in mm^2
        LA_data_['LA'] = LA / (LA_img.shape[0] * LA_img.shape[1] * (GSD*1000)**2)
        LA_data_['gt_bins'] = gt_bins
        LA_data_['gt_bins_norm'] = gt_bins_norm

        if design_label not in LA_data:
            LA_data[design_label] = []

        LA_data[design_label].append(LA_data_)

    # Write trait files
    for design_label, LA in LA_data.items():
        print("Write LA trait csv for", design_label)
        df_LA = pd.DataFrame(LA)
        df_LA['value_json'] = ""

        df_LA['trait'] = "PltLA"
        df_LA['trait_id'] = 39
        df_LA['value'] = df_LA['LA']

        df_LA['timestamp'] = datetime.strptime(campaign_date, "%Y%m%d")
        df_LA['plot.UID'] = df_LA['plot_label']

        df_LA.to_csv(path_trait_csvs / (design_label + "_LA.csv"), index=False)

    for design_label, LA in LA_data.items():
        print("Write bbch30 trait csv for", design_label)
        df_LA = pd.DataFrame(LA)
        df_LA['value_json'] = ""

        df_LA['trait'] = "PltSE"
        df_LA['trait_id'] = 41

        X = [list(d.values()) for d in df_LA['gt_bins_norm']]

        X = sc_x.transform(X)
    
        value = svm_predictor.predict(X)
        #value = -(np.expm1(np.abs(value))) * np.sign(value)
        print("Mean BBCH30 delta:", np.mean(value))

        df_LA['value'] = value

        df_LA['timestamp'] = datetime.strptime(campaign_date, "%Y%m%d")
        df_LA['plot.UID']  = df_LA['plot_label']

        df_LA.to_csv(path_trait_csvs / (design_label + "_BBCH30.csv"), index=False)




def clean_trait_folder(path_campaign, patterns):
    path_trait_csvs = path_campaign / 'trait_csvs'

    files = []
    for pattern in patterns:
        files.extend(path_trait_csvs.glob(pattern))

    for file in files:
        print("deleting", file)
        file.unlink()

def clean_main_folder(path_campaign, patterns):
    path_trait_csvs = path_campaign

    files = []
    for pattern in patterns:
        files.extend(path_trait_csvs.glob(pattern))

    for file in files:
        print("deleting", file)
        file.unlink()