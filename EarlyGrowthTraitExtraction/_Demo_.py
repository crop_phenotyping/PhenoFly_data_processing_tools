import os
from pathlib import Path

from EarlyGrowthTraitExtraction import TraitExtractor

# Paths and settings for sample campaign
########################################

base_dir = Path(os.path.join(os.path.dirname(os.path.realpath(__file__))))
design_label = "FPWW025_lot2"
path_files = base_dir / ".." / "_TestData" / "sonyA9" / "20190402" / "28m_M600P"

# Ground sampling distance
GSD = 0.003  # m
# Buffer to apply before sampling
buffer = round(-0.3 / GSD)  # 0.3 m buffer
# GSD of multi-view images
GSD_output = 0.001  # m
# Size of plot in pixel
ny, nx = (round((2 - (2 * 0.3)) / GSD_output), round((1.5 - (2 * 0.3)) / GSD_output))


# Extract plant counts
######################

TraitExtractor.process_plant_count(
    path_files,
    delta_to_BBCH30=10,
    GSD = GSD)

# Extract CC and BBCH 30 estimations
#############################

TraitExtractor.process_NadirCC_LCCC_LA_BBCH30(
    path_files,
    GSD = GSD)

# Extract tiller counts
######################

TraitExtractor.process_tiller_count(
    path_files,
    path_BBCH30_estimation= path_files / "trait_csvs" / "FPWW025_lot2_BBCH30.csv",
    design=design_label)