&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)
# Utils

This module holds a collection of scripts for various tasks.

### Plant position marker for FIP images

The plant position marker for FIP images allows the user to mark plants in high-resolution FIP images based on counts that were previously made in-field.

See ```Util/FIPImageReferencing```

Two steps are needed:

#### 1. Generate image masks for FIP images
Input:
- Positions of micro GCPs im image coordinates (e.g. ```_TestData/canon_EOS_5D_II/20190222/microGCP_positions.csv```)

Process:
- Run ```Util/FIPImageReferencing/Generate_masks.py```

Output:
- Image masks (e.g. ```_TestData/canon_EOS_5D_II/20190222/masks```)

#### 2. Use GUI for plant position marking

Input:
- FIP preview images (e.g. ```_TestData/canon_EOS_5D_II/20190222/previews```)
- Image masks (e.g. ```_TestData/canon_EOS_5D_II/20190222/masks```)
- Field measurement plant counts (e.g. ```_TestData/canon_EOS_5D_II/20190222/plant_counts.csv```)

Process:
- Run ```Util/FIPImageReferencing/Reference_plants_in_FIP_images.py```

Output:
- Plant position coordinates as relative plot position values (e.g. ```_TestData/canon_EOS_5D_II/20190222/FPWW0240037_plant_coords.csv```)

