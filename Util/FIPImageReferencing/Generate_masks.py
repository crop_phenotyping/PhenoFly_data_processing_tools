import numpy as np
import pandas as pd
import geopandas as gpd
import math
from shapely.geometry import Polygon

a = 0.050 # m
b = 0.125 # m

date = '20190222'
path_base = '_TestData/canon_EOS_5D_II/20190222'

x_ = np.repeat(np.arange(start=a, stop=a*20, step=a),4)
x = np.concatenate([[0, 0], x_, [a*20, a*20]]).reshape(-1,4)
y = np.tile([-b/2, b/2, b/2, -b/2], [20, 1])

sectors = np.stack([x, y], axis=2)

coords = pd.read_csv(path_base + '/microGCP_positions.csv')
coords['plot_label_b'] = coords['Label'].str[0:11]

plot_groups = coords.groupby("plot_label_b")

for plot_label_b, plot_group in plot_groups:
    x_diff = plot_group['X'].iat[1] - plot_group['X'].iat[0]
    y_diff = plot_group['Y'].iat[1] - plot_group['Y'].iat[0]

    # scale sectors
    dist = math.sqrt(x_diff**2 + y_diff**2)
    sectors_ = sectors * dist
    # rotate
    rot = -math.atan2(y_diff, x_diff)
    rot_mat = np.array([[np.cos(rot), -np.sin(rot)], [np.sin(rot), np.cos(rot)]])

    sectors_ = np.dot(sectors_.reshape(-1,2), rot_mat)
    sectors_ = sectors_.reshape(20,4,2)

    # move to origin
    origin = (plot_group['X'].iat[0], plot_group['Y'].iat[0])
    sectors_ = sectors_ + origin

    polygons = []
    datas = []
    for i in range(sectors_.shape[0]):
        sector = sectors_[i,:,:]
        datas.append({'image':plot_label_b, 'plot_label':plot_label_b + "_" + str(i+1), 'type':'soil', 'azimuth_angle':0, 'zenith_angle':math.pi})

        polygons.append(Polygon([(sector[0, 0], sector[0, 1]),
                     (sector[1, 0], sector[1, 1]),
                     (sector[2, 0], sector[2, 1]),
                     (sector[3, 0], sector[3, 1])]))

    datas.append(
        {'image': plot_label_b, 'plot_label': plot_label_b, 'type': 'soil', 'azimuth_angle': 0,
         'zenith_angle': math.pi})
    polygons.append(Polygon([(sectors_[0, 0, 0], sectors_[0, 0, 1]),
                             (sectors_[0, 1, 0], sectors_[0, 1, 1]),
                             (sectors_[19, 3, 0], sectors_[19, 2, 1]),
                             (sectors_[19, 3, 0], sectors_[19, 3, 1])]))

    # Geopandas df to allow geojson export
    gpd_coords = gpd.GeoDataFrame(
        datas, geometry=polygons)

    gpd_coords.to_file(path_base + '/masks/' + "_" + plot_label_b + ".geojson", driver='GeoJSON')







