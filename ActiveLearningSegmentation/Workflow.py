
import matplotlib as mpl
mpl.use('Qt5Agg')
from matplotlib import pyplot as plt, gridspec
from matplotlib.path import Path as GeoPath
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn import metrics

from progressbar import ProgressBar
import multiprocessing
from multiprocessing import Process, Manager
from multiprocessing import Pool

from distutils.dir_util import copy_tree

import sys

from scipy import stats
from scipy.interpolate import griddata

import geopandas as gpd
import gdal
import numpy as np
import pandas as pd
import imageio
import os
import shutil
import time
import timeit
import pickle

from pathlib import Path
import matplotlib.patches as patches

from Common.GISfunctions import create_nband_GeoTiff

# Plot elements
PLOT_CORR = 2
PLOT_ML = 4
PLOT_RGB = 1
PLOT_SEG_PLANT = 3
PLOT_SEG_SOIL = 5


def prepare_descriptor_worker(work_queue, result, read_raw_func, enhanced_raw_func, preview_func, path_workspace_descriptors,
                              path_workspace_previews, border_pixel):
    xtime = np.random.random() * 20
    time.sleep(xtime)

    for job in iter(work_queue.get, 'STOP'):
        image_name = job['image_name']
        image_path = job['image_path']

        raw_file = read_raw_func(str(image_path))

        all_descriptors, descriptor_names, raw_mask = enhanced_raw_func(raw_file, border_pixel)
        image_8bit = preview_func(raw_file, border_pixel)

        np.save(path_workspace_descriptors / (image_name + '.npy'), all_descriptors)

        np.save(path_workspace_descriptors / (image_name + '_mask.npy'), raw_mask)

        imageio.imsave(str(path_workspace_previews / (image_name + '.tif')), image_8bit)

        result.put(descriptor_names)


class Workflow():
    def __init__(self, path_RAW, raw_extension, path_masks, path_upload, path_workspace, plot_group_label_func,
                 read_raw_func, enhanced_raw_func, preview_func,
                 minimum_group_size, sample_type, border_pixels, incremental_run=False):
        '''
        Constructor
        '''
        self.path_RAW = Path(path_RAW)
        self.raw_extension = raw_extension
        self.path_masks = Path(path_masks)
        self.path_upload = Path(path_upload)
        self.path_workspace = Path(path_workspace)
        self.path_workspace_previews = self.path_workspace / "Previews"
        self.path_workspace_descriptors = self.path_workspace / "Descriptors"
        self.path_workspace_predicts = self.path_workspace / "Predicts"
        self.path_workspace_masks = self.path_workspace / "Masks"
        self.path_workspace_RAW = self.path_workspace / "RAW"
        self.plot_group_label_func = plot_group_label_func
        self.minimum_group_size = minimum_group_size
        self.sample_type = sample_type
        self.border_pixels = border_pixels
        self.read_raw_func = read_raw_func
        self.enhanced_raw_func = enhanced_raw_func
        self.preview_func = preview_func
        self.incremental_run = incremental_run

        self.training_plant_coords = []
        self.training_soil_coords = []
        self.plot_group_sample_image_soil = []

        self.coord_training = None
        self.coord_training_responses = None
        self.file_training = None
        self.file_training_responses = None



    def prepare_workspace(self):
        print("Preparing workspace for:",self.path_RAW)

        shutil.rmtree(self.path_workspace, ignore_errors=True)
        os.mkdir(self.path_workspace)

        os.mkdir(self.path_workspace_descriptors)
        os.mkdir(self.path_workspace_previews)
        os.mkdir(self.path_workspace_predicts)

        print("Copy RAW files to local workspace")
        try:
            shutil.copytree(self.path_RAW, self.path_workspace_RAW)
        except Exception as e:
            print(e)

        print("Copy masks to local workspace")
        try:
            shutil.copytree(self.path_masks, self.path_workspace_masks)
        except Exception as e:
            print(e)

    def upload_predictions(self):
        if self.incremental_run:
            copy_tree(str(self.path_workspace_predicts), str(self.path_upload))
        else:
            if os.path.exists(self.path_upload):
                shutil.rmtree(self.path_upload)
            shutil.copytree(self.path_workspace_predicts, self.path_upload)

    def remove_workspace(self):
        shutil.rmtree(self.path_workspace, ignore_errors=True)

    def prepare_descriptors(self):
        # Process RAWs to enhanced RAW (spatial descriptors etc.)
        # iterate over masks -> read only RAWs if mask exits
        image_masks = self.path_workspace_masks.glob("*.geojson")

        print("Process RAW files")
        image_paths = {}
        for i, image_mask in enumerate(image_masks):
            gpd_mask = gpd.read_file(str(image_mask))
            # Extract coherent plot groups labels
            gpd_mask['plot_group_label'] = self.plot_group_label_func(gpd_mask)
            # Extract name of image to read
            image_name = gpd_mask.iloc[0].at['image']

            # Test if prediction already
            if self.incremental_run:
                predicted_image_path = self.path_upload / (image_name + '_predict.tif')
                if predicted_image_path.exists():
                    print("Image", image_name, "already predicted, skip")
                    continue

            image_path = self.path_workspace_RAW / (image_name + self.raw_extension)
            if not image_path.exists():
                print(image_name, "not found, tying with wildcard")
                image_paths_ = list(self.path_workspace_RAW.glob(image_name + "*" + self.raw_extension))
                if len(image_paths_) > 0:
                    image_path = image_paths_[0]
                else:
                    continue

            image_paths[image_name] = image_path

        if len(image_paths)>0:
            # Job and results queue
            m = Manager()
            jobs = m.Queue()
            results = m.Queue()
            processes = []
            # Progress bar counter
            max_jobs = len(image_paths)
            count = 0

            # Build up job queue
            for image_name, image_path in image_paths.items():
                print(image_name, "to queue")

                job = dict()
                job['image_name'] = image_name
                job['image_path'] = image_path
                jobs.put(job)

            # Init progress bar
            progress = ProgressBar(min_value=0, max_value=max_jobs)

            # Start processes, number of CPU - 1 to have some left for main thread / OS
            number_of_cpu = multiprocessing.cpu_count() - 1

            for w in range(number_of_cpu):
                p = Process(target=prepare_descriptor_worker,
                            args=(jobs, results, self.read_raw_func, self.enhanced_raw_func,
                                  self.preview_func,
                                  self.path_workspace_descriptors,
                                  self.path_workspace_previews,
                                  self.border_pixels))
                p.daemon = True
                p.start()
                processes.append(p)
                jobs.put('STOP')

            print("jobs all started")
            progress.update(0)

            # Get results and increment counter with it
            while count < (max_jobs):
                descriptor_names = results.get()

                progress.update(count)
                count += 1

            progress.finish()

            for p in processes:
                p.join()

            np.save(self.path_workspace_descriptors / ('descriptor_names.npy'),descriptor_names)

    def init_images(self):
        df_images = pd.DataFrame()

        image_masks = self.path_workspace_masks.glob("*.geojson")
        for i, image_mask in enumerate(image_masks):
            # Dict so store data
            images_data = {}
            # Read image mask as geopandas
            gpd_mask = gpd.read_file(str(image_mask))
            # Extract coherent plot groups labels
            gpd_mask['plot_group_label'] = self.plot_group_label_func(gpd_mask)
            # Extract name of image to read
            image_name = gpd_mask.iloc[0].at['image']
            # Read preview file

            # Test if prediction already
            if self.incremental_run:
                predicted_image_path = self.path_upload / (image_name + '_predict.tif')
                if predicted_image_path.exists():
                    print("Image", image_name, "already predicted, skip")
                    continue

            try:
                image_8bit = imageio.imread(self.path_workspace_previews / (image_name + '.tif'))
            except FileNotFoundError:
                print('\n' + image_name + '.tif not found, skip')
                continue

            # Store file name
            images_data['id'] = i
            images_data['image_name'] = image_name

            print("\nAdding image " + image_name, end="")

            df_images = df_images.append(images_data, ignore_index=True)

        if len(df_images)>0:
            df_images.set_index('id')

            self.df_images = df_images
        else:
            self.df_images = None

    def init_sample_masks(self):

        j, k = 0, 0
        plot_group_sample_image = {}
        df_images = pd.DataFrame()
        df_plot_groups_samples = pd.DataFrame()
        df_plot_samples = pd.DataFrame()

        image_masks = self.path_workspace_masks.glob("*.geojson")
        for i, image_mask in enumerate(image_masks):
            # Dict so store data
            images_data = {}
            # Read image mask as geopandas
            gpd_mask = gpd.read_file(str(image_mask))
            # Extract coherent plot groups labels
            gpd_mask['plot_group_label'] = self.plot_group_label_func(gpd_mask)
            # Extract name of image to read
            image_name = gpd_mask.iloc[0].at['image']
            # Read preview file

            # Test if prediction already
            if self.incremental_run:
                predicted_image_path = self.path_upload / (image_name + '_predict.tif')
                if predicted_image_path.exists():
                    print("Image", image_name, "already predicted, skip")
                    continue

            try:
                image_8bit = imageio.imread(self.path_workspace_previews / (image_name + '.tif'))
            except FileNotFoundError:
                print('\n' + image_name + '.tif not found, skip')
                continue

            # Store file name
            images_data['id'] = i
            images_data['image_name'] = image_name

            print("\nProcessing mask for " + image_name, end="")

            df_images = df_images.append(images_data, ignore_index=True)

            # Group by coherent plots
            coherent_plot_group_samples = gpd_mask.groupby("plot_group_label")
            # Process coherent plots groups
            for plot_group_label, coherent_plot_group_sample in coherent_plot_group_samples:
                # Only process if minimum number of plots are visible on image
                if len(coherent_plot_group_sample) >= self.minimum_group_size:
                    sys.stdout.write(".")
                    sys.stdout.flush()
                    # Dict to store data
                    plot_group_sample_data = {}
                    # Get bounding box
                    bounding_box = [
                        int(np.min(coherent_plot_group_sample.bounds[['minx']], axis=0)),
                        int(np.max(coherent_plot_group_sample.bounds[['maxx']], axis=0)),
                        int(np.min(coherent_plot_group_sample.bounds[['miny']], axis=0)),
                        int(np.max(coherent_plot_group_sample.bounds[['maxy']], axis=0))
                    ]
                    plot_group_sample_data['bounding_box'] = bounding_box
                    # Add 8bit image for use in GUI
                    plot_group_sample_image[j] = image_8bit[bounding_box[2]:bounding_box[3],
                                                 bounding_box[0]:bounding_box[1], :]
                    # Label and Index
                    plot_group_sample_data['plot_group_label'] = plot_group_label
                    plot_group_sample_data['image_index'] = i
                    plot_group_sample_data['id'] = j

                    df_plot_groups_samples = df_plot_groups_samples.append(plot_group_sample_data,
                                                                           ignore_index=True)

                    # Process single plot sample of coherent plots groups samples
                    for _, plot_sample in coherent_plot_group_sample.iterrows():
                        if plot_sample['type'] == self.sample_type:
                            # Process sample
                            # print("Sample for plot", plot_sample['plot_label'], "found")
                            # Dict to store data
                            plot_sample_data = {}
                            # Create sample name
                            plot_sample_data['plot_label'] = plot_sample['plot_label']
                            # Get path of plot
                            path = GeoPath([a for a in plot_sample.geometry.exterior.coords])
                            plot_sample_data['plot_shape'] = path
                            # Get type
                            plot_sample_data['plot_type'] = plot_sample['type']
                            # Get angles
                            plot_sample_data['zenith_angle'] = plot_sample['zenith_angle']
                            plot_sample_data['azimuth_angle'] = plot_sample['azimuth_angle']
                            # Set segmentation distance to Inf
                            plot_sample_data['segmentation_distance'] = np.Inf
                            plot_sample_data['canopy_coverage'] = np.NaN
                            # Index of group and image the plot sample belongs to
                            plot_sample_data['plot_group_index'] = j
                            plot_sample_data['image_index'] = i
                            plot_sample_data['id'] = k

                            df_plot_samples = df_plot_samples.append(plot_sample_data, ignore_index=True)
                            k += 1
                    j += 1

        df_images.set_index('id')
        df_plot_groups_samples.set_index('id')
        df_plot_samples.set_index('id')

        self.df_images = df_images
        self.df_plot_groups_samples = df_plot_groups_samples
        self.df_plot_samples = df_plot_samples
        self.plot_group_sample_image = plot_group_sample_image

    def save_images_to_workspace(self):
        self.df_images.to_pickle(self.path_workspace / "df_images.pkl")

    def save_masks_to_workspace(self):
        self.df_images.to_pickle(self.path_workspace / "df_images.pkl")
        self.df_plot_groups_samples.to_pickle(self.path_workspace / "df_plot_groups_samples.pkl")
        self.df_plot_samples.to_pickle(self.path_workspace / "df_plot_samples.pkl")
        with open(str(self.path_workspace / "plot_group_sample_image.pkl"), 'wb') as f:
            pickle.dump(self.plot_group_sample_image, f, pickle.HIGHEST_PROTOCOL)

    def load_images_from_workspace(self):
        self.df_images = pd.read_pickle(self.path_workspace / "df_images.pkl")

    def load_masks_from_workspace(self):
        self.df_images = pd.read_pickle(self.path_workspace / "df_images.pkl")
        self.df_plot_groups_samples = pd.read_pickle(self.path_workspace / "df_plot_groups_samples.pkl")
        self.df_plot_samples = pd.read_pickle(self.path_workspace / "df_plot_samples.pkl")
        with open(str(self.path_workspace / "plot_group_sample_image.pkl"), 'rb') as f:
           self.plot_group_sample_image = pickle.load(f)

    def init_new_classifier(self):
        self.clf = RandomForestClassifier(n_estimators=200, max_depth=100, random_state=1, n_jobs=-1, min_samples_split=5)

    def init_RF_classifier_with_fixed_parames(self):

        self.clf = RandomForestClassifier(bootstrap = False,
                                          max_depth = 95,
                                          max_features = 6,
                                          min_samples_leaf = 6,
                                          min_samples_split = 4,
                                          n_estimators = 55,
                                          random_state=1, n_jobs=-1)

    def init_SVM_classifier_with_fixed_parames(self):
        self.clf = svm.OneClassSVM(gamma='scale', kernel='rbf')

    def load_training_coords_from_file(self, path_training_coords_plants, path_training_coords_soil):
        '''Init the training data'''

        df_training_plant_coords = pd.read_csv(path_training_coords_plants)
        self.training_plant_coords = df_training_plant_coords.to_dict('records')

        df_training_soil_ccords = pd.read_csv(path_training_coords_soil)
        self.training_soil_coords = df_training_soil_ccords.to_dict('records')

    def load_training_data_from_file(self, path_training, path_training_responses, overwrite_existing=True):

        training = np.load(path_training)
        training_responses = np.load(path_training_responses)

        if overwrite_existing or self.file_training is None:
            self.file_training = training
            self.file_training_responses = training_responses
        else:
            self.file_training = np.concatenate((self.file_training, training), axis=0)
            self.file_training_responses = np.concatenate((self.file_training_responses, training_responses), axis=0)

    def load_training_data_from_csv(self, path_csv, overwrite_existing=True):

        trainings = pd.read_csv(path_csv)

        training = np.array(trainings[['rawR','rawG','rawB','rawR_diff','rawB_diff','X','Y','Z','sR','sG','sB','H','S','V','L','a','b','ExR','ExG']])
        training_responses = np.array(trainings['label'])

        if overwrite_existing or self.file_training is None:
            self.file_training = training
            self.file_training_responses = training_responses
        else:
            self.file_training = np.concatenate((self.file_training, training), axis=0)
            self.file_training_responses = np.concatenate((self.file_training_responses, training_responses), axis=0)


    def init_training_data_from_coords(self, overwrite_existing=True):

        # Prepare one dataframe with all coords
        df_training_plants = pd.DataFrame().from_records(self.training_plant_coords)
        df_training_plants['type'] = "plant"
        df_training_soil = pd.DataFrame().from_records(self.training_soil_coords)
        df_training_soil['type'] = "soil"
        df_training = pd.concat([df_training_plants, df_training_soil])

        # Prepare array with training pixels
        training_plants = []
        training_soil = []

        # Iterate per image
        training_groups = df_training.groupby("image_name")
        for image_name, training_group in training_groups:
            # load numpy array of image
            enhanced_raw = np.load(self.path_workspace_descriptors / (image_name + '.npy'))
            mask = np.load(self.path_workspace_descriptors / (image_name + '_mask.npy'))
            # extract all plot samples
            for j, sample in training_group.iterrows():
                # Consider mask (e.g. Bayer pattern), take only not masked pixels

                y_image = round(sample['y_image'])
                x_image = round(sample['x_image'])

                if not mask[y_image, x_image]:
                    training = enhanced_raw[y_image, x_image].tolist()

                    if sample['type'] == 'soil':
                        training_soil.append(training)
                    else:
                        training_plants.append(training)
        training_plants = np.array(training_plants)
        training_soil = np.array(training_soil)

        # Combine trainings
        training = np.array(np.concatenate((training_plants, training_soil)), dtype=np.float32)
        # Create response arrays
        training_responses_plants = np.full((training_plants.shape[0]), 1)
        training_responses_soil = np.full((training_soil.shape[0]), 0)
        training_responses = np.array(np.append(training_responses_plants, training_responses_soil), dtype=np.int8)

        if overwrite_existing or self.coord_training is None:
            self.coord_training = training
            self.coord_training_responses = training_responses
        else:
            self.coord_training = np.concatenate((self.coord_training, training), axis=0)
            self.coord_training_responses = np.concatenate((self.coord_training_responses, training_responses), axis=0)

    def save_training_data_from_coords(self):
        np.save(self.path_workspace / "Training.npy", self.coord_training)
        np.save(self.path_workspace / "Training_response.npy", self.coord_training_responses)

    def get_training(self):
        if self.file_training is not None and self.coord_training is not None:
            print("combining file and coordinate based training")
            training = np.concatenate((self.file_training, self.coord_training), axis=0)
            training_response = np.concatenate((self.file_training_responses, self.coord_training_responses),
                                               axis=0)
        elif self.file_training is not None:
            print("file based training")
            training = self.file_training
            training_response = self.file_training_responses
        else:
            print("coord based training")
            training = self.coord_training
            training_response = self.coord_training_responses

        return(training, training_response)

    def train_classifier(self):
        training, training_response = self.get_training()

        self.clf.fit(training, training_response)
        if 'feature_importances_' in self.clf.__dict__:
            if (self.path_workspace_descriptors / 'descriptor_names.npy').exists():
                predictor_names = np.load(self.path_workspace_descriptors / ('descriptor_names.npy'))
                print(predictor_names)
            print(self.clf.feature_importances_)

    def segment_image_cutouts(self):
        print("Predict image cutouts")

        self.plot_group_sample_image_plants = {}
        self.plot_group_sample_image_soil = {}

        # iterate over images
        samples_by_image_groups = self.df_plot_samples.groupby("image_index")
        for image_index, samples_by_image in samples_by_image_groups:
            image = self.df_images.loc[image_index]

            # load numpy array
            enhanced_raw = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '.npy'))
            enhanced_raw = np.delete(enhanced_raw, 19, axis=2)
            mask = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '_mask.npy'))

            # iterate over plot groups on image
            samples_by_group = samples_by_image.groupby("plot_group_index")
            for plot_group_index, sample_group in samples_by_group:

                # get RGB cutout
                image_rgb_cutout = self.plot_group_sample_image[plot_group_index]
                # get RAW cutout
                image_bounding_box = self.df_plot_groups_samples.loc[plot_group_index].at['bounding_box']
                enhanced_raw_cutout = enhanced_raw[
                                      image_bounding_box[2]:image_bounding_box[3],
                                      image_bounding_box[0]:image_bounding_box[1]]
                mask_cutout = mask[
                              image_bounding_box[2]:image_bounding_box[3],
                              image_bounding_box[0]:image_bounding_box[1]]

                # predict cutout part
                #image_proba_cutout = self.clf.predict_proba(enhanced_raw_cutout.reshape(-1, enhanced_raw_cutout.shape[-1]))
                #image_proba_cutout = image_proba_cutout.reshape((enhanced_raw_cutout.shape[0], enhanced_raw_cutout.shape[1], 2))

                # Round to 0 (soil) or 1 (plants)
                #image_seg_cutout = image_proba_cutout[:,:,0] > image_proba_cutout[:,:,1]
                image_seg_cutout = self.clf.predict(enhanced_raw_cutout.reshape(-1, enhanced_raw_cutout.shape[-1])).reshape((enhanced_raw_cutout.shape[0], enhanced_raw_cutout.shape[1]))
                image_seg_cutout = np.uint8(np.round(image_seg_cutout))

                # Interpolate masked pixel positions (e.g. based on Bayer pattern)
                x, y = np.indices(image_seg_cutout.shape)
                image_seg_cutout = griddata((x[~mask_cutout], y[~mask_cutout]), image_seg_cutout[~mask_cutout], (x, y),
                                            method='linear')
                image_seg_cutout[np.isnan(image_seg_cutout)] = 0

                # Mask RGB in plants / soil for GUI
                mask_seg_rgb_cutout = np.stack(np.tile(image_seg_cutout > 0, [3, 1, 1]), axis=2)
                rgb_plants_cutout = np.ma.masked_array(image_rgb_cutout, mask_seg_rgb_cutout)
                rgb_soil_cutout = np.ma.masked_array(image_rgb_cutout, np.logical_not(mask_seg_rgb_cutout))

                self.plot_group_sample_image_plants[plot_group_index] = rgb_plants_cutout
                self.plot_group_sample_image_soil[plot_group_index] = rgb_soil_cutout

                # Step 1: Collect data for all plots
                plot_samples = self.df_plot_samples[self.df_plot_samples.plot_group_index == plot_group_index]
                for _, sample in plot_samples.iterrows():
                    # Test which pixels are part of plot
                    pixel_positions_cutout = np.indices(image_seg_cutout.shape).reshape(2, -1)
                    pixel_positions_image = np.copy(pixel_positions_cutout)
                    pixel_positions_image = np.flip(np.swapaxes(pixel_positions_image, 0, 1), 1)
                    pixel_positions_image[:, 0], pixel_positions_image[:, 1] = pixel_positions_image[:, 0] + image_bounding_box[0], pixel_positions_image[:, 1] + \
                                                                               image_bounding_box[2]

                    # Get image pixel positions that are part of plot
                    hits = sample.plot_shape.contains_points(pixel_positions_image)
                    # If positions found: calculate canopy coverage
                    if pixel_positions_cutout[:, hits].shape[1] > 0:
                        plot_shape = image_seg_cutout[pixel_positions_cutout[0, hits], pixel_positions_cutout[1, hits]]
                        canopy_coverage = np.sum(plot_shape) / len(plot_shape)
                        self.df_plot_samples.at[sample.id, 'canopy_coverage'] = canopy_coverage
                sys.stdout.write('.')
                sys.stdout.flush()

        # Step 2: Calculate linear regressions per plot
        plots = pd.unique(self.df_plot_samples['plot_label'])
        for plot in plots:
            # Get corresponding canopy coverage value for plots
            plot_samples = self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot,]
            if len(plot_samples)>1:
                # Calculate linear reression
                slope, intercept, r_value, p_value, std_err = stats.linregress(plot_samples['zenith_angle'], plot_samples['canopy_coverage'])

                # Save values
                self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'slope'] = slope
                self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'intercept'] = intercept
                self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'r2'] = r_value
                self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'predict'] = intercept + \
                                                                                                  slope * self.df_plot_samples.loc[
                                                                                                      self.df_plot_samples[
                                                                                                          'plot_label'] == plot, 'zenith_angle']
                # Calculate distance of points to linear regression line
                self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'segmentation_distance'] = \
                    np.abs(self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'canopy_coverage'] - \
                    self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == plot, 'predict'])

        print("Done")

    def segment_images(self):
        if self.df_images is not None:
            # iterate over images
            for index, row in self.df_images.iterrows():

                tic = timeit.default_timer()

                image = row
                print("Predict " + image.at['image_name'])

                # load numpy array
                print("Load...", end="")
                enhanced_raw = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '.npy'))
                enhanced_raw = np.delete(enhanced_raw, 19, axis=2)

                mask = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '_mask.npy'))
                print(timeit.default_timer() - tic, "seconds")

                print("Predict...", end="")
                # Round to 0 (soil) or 1 (plants)
                image_seg = self.clf.predict(enhanced_raw.reshape(-1, enhanced_raw.shape[-1])).reshape((enhanced_raw.shape[0], enhanced_raw.shape[1]))
                print(timeit.default_timer() - tic, "seconds")

                print("Postrocess...", end="")
                image_seg = np.uint8(np.round(image_seg) * 255)
                image_seg = np.ma.masked_array(image_seg, mask, fill_value=128).filled()
                print(timeit.default_timer() - tic, "seconds")

                print("Save...", end="")
                imageio.imsave(self.path_workspace_predicts / (image.at['image_name'] + '_predict.tif'), image_seg)
                print(timeit.default_timer() - tic, "seconds")

                print("Done")

        else:
            print("No images to segment")

            
    def predict_image(self, args):
        index, row = args
        image = row
        print("Predict " + image.at['image_name'])

        # load numpy array
        print("Load...")
        enhanced_raw = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '.npy'))

        mask = np.load(self.path_workspace_descriptors / (image.at['image_name'] + '_mask.npy'))

        print("Predict...")
        # Round to 0 (soil) or 1 (plants)
        image_seg = self.clf.predict(enhanced_raw.reshape(-1, enhanced_raw.shape[-1])).reshape(
            (enhanced_raw.shape[0], enhanced_raw.shape[1]))
        print("Postrocess...")
        image_seg = np.uint8(np.round(image_seg) * 255)
        image_seg = np.ma.masked_array(image_seg, mask, fill_value=128).filled()

        print("Save...")
        imageio.imsave(self.path_workspace_predicts / (image.at['image_name'] + '_predict.tif'), image_seg)

        print("Done")



    def save_training_coords_to_file(self):
        # Save training files
        df_training_plants = pd.DataFrame().from_records(self.training_plant_coords)
        df_training_soil = pd.DataFrame().from_records(self.training_soil_coords)
        df_training_plants.to_csv(self.path_workspace / "_training_plant_coords.csv")
        df_training_soil.to_csv(self.path_workspace / "_training_soil_coords.csv")


    def GUI(self):
        # Plot is interactive
        plt.interactive(True)
        # List for plot elements
        axs = [None, None, None, None, None, None]

        # Generate figure
        fig = plt.figure(figsize=(10, 4))
        # 3 x 2 row / column layout
        gs = gridspec.GridSpec(3, 2, width_ratios=[1, 5])

        # Preset first dataset to load on start
        active_dataset = {'button': 0,
                          'image_id':0, 'plot_group_sample_id':0, 'plot_sample_id': 0,
                          'image_name': None, 'plot_group_label': None, 'plot_label': None}


        # Drawing functions
        def draw_training_points(image_name, plot_group_label):
            # Draw marker on clicked position
            # Extract coordinates
            veg_points = [x for x in self.training_plant_coords if x['image_name'] == image_name and x['plot_group_label'] == plot_group_label]
            soil_points = [x for x in self.training_soil_coords if x['image_name'] == image_name and x['plot_group_label'] == plot_group_label]

            fig.canvas.draw()
            if len(veg_points) > 0: axs[PLOT_RGB].scatter('x', 'y', data=pd.DataFrame(veg_points), marker='+', color='white')
            if len(soil_points) > 0: axs[PLOT_RGB].scatter('x', 'y', data=pd.DataFrame(soil_points), marker='+', color='red')

            if axs[PLOT_SEG_PLANT]:
                if len(veg_points) > 0: axs[PLOT_SEG_PLANT].scatter('x', 'y', data=pd.DataFrame(veg_points), marker='+', color='white')
                if len(soil_points) > 0: axs[PLOT_SEG_PLANT].scatter('x', 'y', data=pd.DataFrame(soil_points), marker='+', color='red')
            if axs[PLOT_SEG_SOIL]:
                if len(veg_points) > 0: axs[PLOT_SEG_SOIL].scatter('x', 'y', data=pd.DataFrame(veg_points), marker='+', color='white')
                if len(soil_points) > 0: axs[PLOT_SEG_SOIL].scatter('x', 'y', data=pd.DataFrame(soil_points), marker='+', color='red')


        # GUI event handler

        # In click: add or delete training points
        def onclick(event):
            # Coordinates of click
            x = event.xdata
            y = event.ydata

            # sample
            plot_sample = self.df_plot_samples.loc[active_dataset['plot_sample_id']]
            plot_group = self.df_plot_groups_samples.loc[plot_sample.at['plot_group_index']]
            x_image = x + plot_group.at['bounding_box'][0]
            y_image = y + plot_group.at['bounding_box'][2]

            # Button 1: Training point for plants added
            if event.button == 1:
                self.training_plant_coords.append({'image_name': active_dataset['image_name'],
                                                   'plot_group_label': active_dataset['plot_group_label'],
                                                   'plot_label': active_dataset['plot_label'],
                                                   'x': x, 'y': y,
                                                   'x_image':x_image, 'y_image':y_image})
                active_dataset['button'] = 1
            # Button 3: Training point for soil added
            elif event.button == 3:
                self.training_soil_coords.append({'image_name': active_dataset['image_name'],
                                                  'plot_group_label': active_dataset['plot_group_label'],
                                                  'plot_label': active_dataset['plot_label'],
                                                  'x': x, 'y': y,
                                                  'x_image': x_image, 'y_image': y_image})
                active_dataset['button'] = 3
            # Button 2: Remove last training point
            elif event.button == 2:

                # Reset button
                active_dataset['button'] = 0

                # If last point was plant: Remove plant point from training
                if active_dataset['button'] == 1:
                    del self.training_plant_coords[-1]
                # If last point was soil: Remove soil point from training
                elif active_dataset['button'] == 3:
                    del self.training_soil_coords[-1]

            # Redraw graph
            # Remove allpoints from graph
            del axs[PLOT_RGB].collections[:]

            draw_training_points(active_dataset['image_name'], active_dataset['plot_group_label'])


        # On button pressed: load new images or train algorithm
        def onkey(event):

            if event.key == 'n':
                # Clear plots
                plt.clf()

                # If the model is trained: Select next plot sample from the top 100 with the largest segmentation distance
                if 'segmentation_distance' in self.df_plot_samples:
                    next_plot_samples = self.df_plot_samples.nlargest(100, 'segmentation_distance')
                else:
                    next_plot_samples = self.df_plot_samples

                # Randomly select one plot sample
                plot = next_plot_samples.sample(n=1).iloc[0]
                plot_group = self.df_plot_groups_samples.loc[plot.at['plot_group_index']]
                image = self.df_images.loc[plot_group['image_index']]

                # Repopulate active dataset
                active_dataset.update({'image_id': image['id'], 'plot_group_sample_id': plot_group['id'], 'plot_sample_id': plot['id'],
                                       'image_name': image['image_name'], 'plot_group_label': plot_group['plot_group_label'],
                                       'plot_label': plot['plot_label']})

                # load origin image for UI
                image_rgb = self.plot_group_sample_image[plot_group['id']]

                axs[PLOT_RGB] = fig.add_subplot(gs[PLOT_RGB])
                axs[PLOT_RGB].imshow(image_rgb)

                path = plot.plot_shape
                path_ = path.transformed(mpl.transforms.Affine2D().translate(-plot_group.bounding_box[0], -plot_group.bounding_box[2]))

                patch = patches.PathPatch(path_, facecolor='white', edgecolor='white', fill=False, lw=1)
                axs[PLOT_RGB].add_patch(patch)

                plt.title(f'plot_sample_id: {active_dataset["plot_sample_id"]}, image_name: {active_dataset["image_name"]}, plot label: {active_dataset["plot_label"]}, CC:{np.round(plot["canopy_coverage"], 2)}')

                # draw Plot with correlation
                axs[PLOT_CORR] = fig.add_subplot(gs[PLOT_CORR])
                plot_images = self.df_plot_samples.loc[self.df_plot_samples['plot_label'] == active_dataset['plot_label']]

                if 'predict' in plot_images:
                    axs[PLOT_CORR].scatter('zenith_angle', 'canopy_coverage', label='all', data=plot_images)
                    axs[PLOT_CORR].scatter('zenith_angle', 'canopy_coverage', label='selected', \
                                           data=plot_images.loc[plot_images['image_index'] == active_dataset["image_id"]], color="red")
                    axs[PLOT_CORR].plot(plot_images['zenith_angle'], plot_images['predict'], 'r', label='fitt')

                # Redraw plots
                plt.draw()
                # load segmented image for UI
                if len(self.plot_group_sample_image_soil)>0:
                    soil = self.plot_group_sample_image_soil[plot_group['id']]
                    plants = self.plot_group_sample_image_plants[plot_group['id']]

                    axs[PLOT_SEG_PLANT] = fig.add_subplot(gs[PLOT_SEG_PLANT])
                    axs[PLOT_SEG_PLANT].imshow(plants.filled())
                    patch = patches.PathPatch(path_, facecolor='white', edgecolor='white', fill=False, lw=1)
                    axs[PLOT_SEG_PLANT].add_patch(patch)

                    axs[PLOT_SEG_SOIL] = fig.add_subplot(gs[PLOT_SEG_SOIL])
                    axs[PLOT_SEG_SOIL].imshow(soil.filled())
                    patch = patches.PathPatch(path_, facecolor='white', edgecolor='white', fill=False, lw=1)
                    axs[PLOT_SEG_SOIL].add_patch(patch)

                    # Plot feature inportance
                    importances = self.clf.feature_importances_
                    std = np.std([tree.feature_importances_ for tree in self.clf.estimators_],
                                 axis=0)
                    indices = np.argsort(importances)[::-1]

                    # Plot the feature importances of the forest
                    predictor_names = np.load(self.path_workspace_descriptors / ('descriptor_names.npy'))

                    axs[PLOT_ML] = fig.add_subplot(gs[PLOT_ML])
                    axs[PLOT_ML].bar(predictor_names, importances[indices],
                                     color="r", yerr=std[indices], align="center")
                    #axs[PLOT_ML].xticks(range(predictor_names), indices)
                    #axs[PLOT_ML].xlim([-1, len(predictor_names)])

                draw_training_points(active_dataset['image_name'], active_dataset['plot_group_label'])


            if event.key == "c":

                self.init_training_data_from_coords()
                self.train_classifier()

                self.segment_image_cutouts()

            if event.key == "d":

                self.save_training_coords_to_file()

        fig.canvas.mpl_connect('button_press_event', onclick)
        fig.canvas.mpl_connect('key_press_event', onkey)



