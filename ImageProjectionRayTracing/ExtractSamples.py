##############################################################################
#
# Project: PhenoFly UAS data processing tools
# File: Extract samples from direct georeferenced images using a samples mask
#
# Author: Lukas Roth (lukas.roth@usys.ethz.ch)
#
# Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##############################################################################


#
# Input:
# - RAW images
# - Sample mask geoJSON
# - X/Y coordinates TIFF per image
# - Zenith/Azimuth angle TIFF per image
#
# Output:
# - Sampled image files as georeferenced geoTIFF including 1bit mask
# - Zenith/Azimuth angle TIFF as georeferenced geoTIFF including 1bit mask
#

# Dependencies
import sys
import argparse
import shutil
import os
import gdal
import numpy as np
from progressbar import ProgressBar
import multiprocessing
from multiprocessing import Process, Manager
from matplotlib import path
import geojson
import math
import pandas as pd
import osr
from affine import Affine

################################################################################
# Helper functions
################################################################################

def extract_worker(work_queue, result_queue, raw_files_folder, metadata_folder, output_directory, samples_polygons, verbose):
    """ Worker to perform extraction of samples from RAW images

    :param work_queue: Work queue with name of RAW images
    :param result_queue: Queue to write pseudo result in if done
    :param metadata_folder: Path to metadata with latitude/londitude and zenith/azimuth data
    :param output_directory: Path to directory to write exported samples
    :param samples_polygons: Polygons of samples to export
    :return:
    """
    for job in iter(work_queue.get, 'STOP'):
        raw_filename_full = job['raw_filename_full']
        raw_filename = job['raw_filename']
        viewpoints = create_extracts(raw_files_folder, metadata_folder, output_directory, samples_polygons, raw_filename_full, raw_filename, verbose=verbose)
        result_queue.put(viewpoints)

def create_extracts(raw_files_folder, metadata_folder, output_directory, samples_polygons, raw_filename_full, raw_filename, test_radius = 0.0001, verbose=False, epsg=2056):
    """Create extracts for RAW images

    :param metadata_folder: Path to metadata with latitude/londitude and zenith/azimuth data
    :param output_directory: Path to directory to write exported samples
    :param samples_polygons: Polygons of samples to export
    :param raw_filename_full: full name (with extension) of RAW image to process
    :param raw_filename: name of RAW image to process
    :param test_radius: Tolerance radius in which a point is considered to be in a polygon
    :return:
    """

    # Viewpoint dataset
    viewpoints = []

    # Open xy coords
    photo_meta_x_ds = gdal.Open(os.path.join(metadata_folder, raw_filename + '_DG_coord_xy.tif'))
    # Get x and offset/scale it to world coords
    band_x = photo_meta_x_ds.GetRasterBand(1)
    photo_meta_x = np.array(band_x.ReadAsArray(), dtype=np.float64)
    offset_x = np.full(photo_meta_x.shape, float(band_x.GetMetadataItem("DG_OFFSET")))
    scale_x = np.full(photo_meta_x.shape, float(band_x.GetMetadataItem("DG_SCALE")))
    photo_meta_x_world = photo_meta_x * scale_x + offset_x
    min_x_world, max_x_world = (np.min(photo_meta_x_world.flat), np.max(photo_meta_x_world.flat))
    # Get y and offset/scale it to world coords
    band_y = photo_meta_x_ds.GetRasterBand(2)
    photo_meta_y = np.array(band_y.ReadAsArray(), dtype=np.float64)
    offset_y = np.full(photo_meta_y.shape, float(band_y.GetMetadataItem("DG_OFFSET")))
    scale_y = np.full(photo_meta_y.shape, float(band_y.GetMetadataItem("DG_SCALE")))
    photo_meta_y_world = photo_meta_y * scale_y + offset_y
    min_y_world, max_y_world = (np.min(photo_meta_y_world.flat), np.max(photo_meta_y_world.flat))

    # Open viewing geometry file
    photo_meta_view_ds = gdal.Open(os.path.join(metadata_folder, raw_filename + '_DG_viewing_geometry.tif'))
    # Get zenith angel, offset/scale
    band_zenith = photo_meta_view_ds.GetRasterBand(1)
    photo_meta_zenith_ = np.array(band_zenith.ReadAsArray(), dtype=np.float64)
    offset_zenith = np.full(photo_meta_zenith_.shape, float(band_zenith.GetMetadataItem("DG_OFFSET")))
    scale_zenith = np.full(photo_meta_zenith_.shape, float(band_zenith.GetMetadataItem("DG_SCALE")))
    photo_meta_zenith = photo_meta_zenith_ * scale_zenith + offset_zenith
    # Get azimuth angle, offset/scale
    band_azimuth = photo_meta_view_ds.GetRasterBand(2)
    photo_meta_azimuth_ = np.array(band_azimuth.ReadAsArray(), dtype=np.float64)
    offset_azimuth = np.full(photo_meta_azimuth_.shape, float(band_azimuth.GetMetadataItem("DG_OFFSET")))
    scale_azimuth = np.full(photo_meta_azimuth_.shape, float(band_azimuth.GetMetadataItem("DG_SCALE")))
    photo_meta_azimuth = photo_meta_azimuth_ * scale_azimuth + offset_azimuth

    # Stack x and y coordinates and reshape to tuples
    photo_meta_xy_world = np.stack((photo_meta_x_world, photo_meta_y_world), axis=2)
    photo_meta_xy_world = photo_meta_xy_world.reshape(-1, 2)

    # init projection and geotiff driver, open RAW file for later processing
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)

    fileformat = "GTiff"
    driver = gdal.GetDriverByName(fileformat)
    photo_raw_ds = gdal.Open(os.path.join(raw_files_folder, raw_filename_full))

    # Prepare empty array to store found plot shapes
    found_plots = np.zeros(shape=photo_meta_x.shape)
    # Find samples in image
    for sample in samples_polygons:
        # Get coordinates and sample number
        coords = sample['geometry']['coordinates'][0]
        sample_no = sample['properties']['plot_label']
        sample_path = path.Path(coords)
        sample_extents_world = sample_path.get_extents().extents

        # Check if extends of sample in image
        if (min_x_world < sample_extents_world[0] and sample_extents_world[2] < max_x_world and min_y_world < sample_extents_world[1] and sample_extents_world[3] < max_y_world):
            # Check if sample is in image
            sample_mask_image = sample_path.contains_points(photo_meta_xy_world, radius=test_radius)
            if np.sum(sample_mask_image) > 0:
                if verbose:
                    print(sample_no, "is part of image", raw_filename, flush=True)

                sample_filename = os.path.join(output_directory, raw_filename + '_' + str(sample_no) + '.tif')
                # Create path out of polygon and mask image
                sample_mask_image = sample_mask_image.reshape(photo_meta_x.shape)
                sample_hits_image = np.where(sample_mask_image)


                plot_extent_image = (np.min(sample_hits_image[0]), np.max(sample_hits_image[0]), np.min(sample_hits_image[1]), np.max(sample_hits_image[1]))

                # Exclude incomplete plot images
                if plot_extent_image[1]-plot_extent_image[0]>0 and plot_extent_image[3]-plot_extent_image[2]>0 \
                    and plot_extent_image[0]>0 and plot_extent_image[2] >0 \
                    and plot_extent_image[1]<(sample_mask_image.shape[0]-1) and plot_extent_image[3] < (sample_mask_image.shape[1]-1):
                    # Copy RAW file, clip and mask it
                    gdal.SetConfigOption('GDAL_TIFF_INTERNAL_MASK', 'TRUE')
                    sample_ds = driver.Create(sample_filename, ysize=int(plot_extent_image[1]-plot_extent_image[0]),
                                              xsize=int(plot_extent_image[3]-plot_extent_image[2]),
                                              bands=photo_raw_ds.RasterCount, eType=gdal.GetDataTypeByName(gdal.GetDataTypeName(photo_raw_ds.GetRasterBand(1).DataType)))
                    # Create mask to save plot pixels in
                    sample_ds.CreateMaskBand(gdal.GMF_PER_DATASET)

                    # Clip image to extends for every band of source file
                    for band_i in range(sample_ds.RasterCount):
                        band_dest = sample_ds.GetRasterBand(band_i + 1)
                        band_arr_src = photo_raw_ds.GetRasterBand(band_i + 1).ReadAsArray()
                        band_arr_dest = band_arr_src[plot_extent_image[0]:plot_extent_image[1], plot_extent_image[2]:plot_extent_image[3]]
                        # Flip both axis
                        band_arr_dest = np.flip(np.flip(band_arr_dest, 0), 1)
                        band_dest.WriteArray(band_arr_dest)
                        # Write mask
                        band_dest.SetNoDataValue(0)
                        band_dest.GetMaskBand().WriteArray(np.flip(np.flip(sample_mask_image[plot_extent_image[0]:plot_extent_image[1], plot_extent_image[2]:plot_extent_image[3]], 0), 1))

                    # Calc rotation of image in relation to North
                    rotation_angle = np.arctan2((photo_meta_y_world[plot_extent_image[1], plot_extent_image[2]] - photo_meta_y_world[plot_extent_image[0], plot_extent_image[2]]), (photo_meta_x_world[plot_extent_image[1], plot_extent_image[2]] - photo_meta_x_world[plot_extent_image[0], plot_extent_image[2]])) + (np.pi/2)

                    pix_size_x = -1 * abs(math.sqrt(
                        (photo_meta_y_world[plot_extent_image[1], plot_extent_image[3]] -
                        photo_meta_y_world[plot_extent_image[1], plot_extent_image[2]]) **2 +
                        (photo_meta_x_world[plot_extent_image[1], plot_extent_image[3]] -
                        photo_meta_x_world[plot_extent_image[1], plot_extent_image[2]])**2) /
                        abs(plot_extent_image[3] - plot_extent_image[2]))

                    pix_size_y = abs(math.sqrt(
                        (photo_meta_y_world[plot_extent_image[0], plot_extent_image[2]] -
                         photo_meta_y_world[plot_extent_image[1], plot_extent_image[2]]) ** 2 +
                        (photo_meta_x_world[plot_extent_image[0], plot_extent_image[2]] -
                         photo_meta_x_world[plot_extent_image[1], plot_extent_image[2]]) ** 2) /
                                  abs(plot_extent_image[0] - plot_extent_image[1]))

                    geotransform = [0, 0, 0, 0, 0, 0]
                    geotransform[0] = photo_meta_x_world[plot_extent_image[1], plot_extent_image[3]]
                    geotransform[1] = pix_size_x * np.cos(rotation_angle)
                    geotransform[2] = - pix_size_y * np.sin(rotation_angle)
                    geotransform[3] = photo_meta_y_world[plot_extent_image[1], plot_extent_image[3]]
                    geotransform[4] = pix_size_x * np.sin(rotation_angle)
                    geotransform[5] = pix_size_y * np.cos(rotation_angle)



                    a_trans = Affine.translation(photo_meta_x_world[plot_extent_image[1], plot_extent_image[3]],
                                                 photo_meta_y_world[plot_extent_image[1], plot_extent_image[3]])

                    a_scale = Affine.scale(pix_size_x)
                    a_rot = Affine.rotation(rotation_angle)

                    a_shear = Affine.shear(45.0, 45.0)
                    a_all = a_trans * a_scale * a_scale * a_rot

                    Affine.to_gdal(a_all)

                    sample_ds.SetGeoTransform(geotransform)
                    sample_ds.SetProjection(srs.ExportToWkt())

                    # Close file to force writing
                    sample_ds = None

                    # Calculate meta data (zenith and azimuth angle, xy sensor position)
                    photo_meta_zenith_masked = np.ma.array(photo_meta_zenith, mask=(np.logical_not(sample_mask_image)))
                    zenith_mean = int(photo_meta_zenith_masked.mean())
                    photo_meta_azimuth_masked = np.ma.array(photo_meta_azimuth, mask=(np.logical_not(sample_mask_image)))
                    azimuth_mean = int(photo_meta_azimuth_masked.mean())
                    sensor_x_mean = int((plot_extent_image[1] - plot_extent_image[0]) / 2.0)
                    sensor_y_mean = int((plot_extent_image[3] - plot_extent_image[2]) / 2.0)

                    # Append to viewpoint dataset
                    viewpoints.append({
                                    'image': raw_filename,
                                    'image_full': raw_filename_full,
                                    'sample_no': sample_no,
                                    'zenith': zenith_mean,
                                    'azimuth': azimuth_mean,
                                    'sensor_x': sensor_x_mean,
                                    'sensor_y': sensor_y_mean})


    # Close file
    photo_raw_ds = None

    return(viewpoints)

###main process routin

def process(path_raw_files, path_meta_files, sample_geojson, output_directory, verbose=False):

    # give short summary of arguments
    print('================================================================================\nExtract samples from direct georeferenced images using a samples mask')
    print('================================================================================')
    print('Paths:\n--------------------------------------------------------------------------------')
    print('RAW files:', path_raw_files)
    print('Meta file:', path_meta_files)
    print('Sample geoJSON:', sample_geojson)
    print('Output folder:', output_directory)
    print('================================================================================')

    ###########################
    # Start

    # Prepare output directory
    # Prepare output directory, delete content
    shutil.rmtree(output_directory, ignore_errors=True)
    os.mkdir(output_directory)

    # Open samples file
    with open(sample_geojson, 'r') as f:
        samples_polygons_ = geojson.load(f)
        samples_polygons = samples_polygons_['features']

    # Job and results queue
    m = Manager()
    jobs = m.Queue()
    results = m.Queue()
    processes = []
    # Progress bar counter
    max_jobs = 0
    count = 0

    # Build up job queue by iterating over RAW files
    directory = os.fsencode(path_raw_files)
    for file in os.listdir(directory):
        # Get filename
        raw_filename_full = os.fsdecode(file)
        raw_filename = os.path.splitext(raw_filename_full)[0]
        # Test if corresponding metadata exist
        if os.path.isfile(os.path.join(path_meta_files, raw_filename + '_DG_coord_xy.tif')) and \
                os.path.isfile(os.path.join(path_meta_files, raw_filename + '_DG_viewing_geometry.tif')):
            # Metadata found, add job to queue
            job = dict()
            job['raw_filename_full'] = raw_filename_full
            job['raw_filename'] = raw_filename
            jobs.put(job)
            max_jobs += 1
    # Init progress bar
    progress = ProgressBar(min_value=0, max_value=max_jobs)

    # Start processes, number of CPU - 2 to have some left for main thread / OS
    for w in range(multiprocessing.cpu_count() - 2):
        p = Process(target=extract_worker, args=(jobs, results, path_raw_files, path_meta_files, output_directory, samples_polygons, verbose))
        p.daemon = True
        p.start()
        processes.append(p)
        jobs.put('STOP')

    print("jobs all started")
    progress.update(0)

     #get viewpoint results, set counter per processed image
    viewpoints = []
    progress.update(0)
    while count < (max_jobs):
        viewpoint_ = results.get()
        viewpoints.extend(viewpoint_)
        progress.update(count)
        count += 1

    progress.finish()

    for p in processes:
        p.join()

    pd_data = pd.DataFrame(viewpoints)
    pd_data.to_csv(os.path.join(output_directory, "viewpoint.csv"))

################################################################################
# Main
################################################################################


def main(argv):
    # Configure command line arguments
    parser = argparse.ArgumentParser(
        description='Extract samples from direct georeferenced images using a samples mask')

    parser.add_argument("output", help="path to output directory")

    parser.add_argument("-r", "--path_raw_files", help="Path to RAW files", required=True)
    parser.add_argument("-m", "--path_meta_files", help="Path to meta files (x/y and zenit/azimuth files)", required=True)
    parser.add_argument("-s", "--sample_geojson", help="geoJSON that contains the sample areas", required=True)
    parser.add_argument("-e", "--epsg", help="reference coordinate system (EPSG number", type=int, default=2056)
    parser.add_argument("-v", "--verbose", action="store_true", help="increase output verbosity")

    args = parser.parse_args()

    # read arguments
    path_raw_files = args.path_raw_files
    path_meta_files = args.path_meta_files
    sample_geojson = args.sample_geojson
    output_directory = args.output
    epsg = args.epsg
    verbose = args.verbose

    process(path_raw_files, path_meta_files, sample_geojson, output_directory, epsg, verbose)



if __name__ == "__main__":
    main(sys.argv[1:])

    print(
        "\n================================================================================\nAll photos processed, END\n")

