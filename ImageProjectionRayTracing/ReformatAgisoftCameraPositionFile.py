##############################################################################
#
# Project: PhenoFly UAS data processing tools
# File: Coverts an Agisoft camera position file to a regular CSV file
#
# Author: Lukas Roth (lukas.roth@usys.ethz.ch)
#
# Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Based on:
# Roth, Aasen, Walter, Liebisch 2018: Extracting leaf area index using viewing
# geometry effects—A new perspective on high-resolution unmanned aerial system
# photography, ISPRS Journal of Photogrammetry and Remote Sensing.
# https://doi.org/10.1016/j.isprsjprs.2018.04.012
#
##############################################################################

# Input:
# - Agisoft camera position file
#
# Output:
# - XYZ Omega Phi Kappa CSV file
#



import csv
import sys
import argparse

def process(agisoft_file, csv_file):
    # read arguments

    with open(agisoft_file, 'r') as infile:
        with open(csv_file, 'w') as outfile:
            csvreader = csv.reader(infile, delimiter='\t', quotechar='|')
            next(csvreader, None)
            next(csvreader, None)

            csvwriter = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow(
                ["image", "X", "Y", "Z", "Omega", "Phi", "Kappa", "r11", "r12", "r13", "r21", "r22", "r23", "r31",
                 "r32", "r33"])

            csvwriter.writerows(row for row in csvreader)


def main(argv):
    # Configure command line arguments
    parser = argparse.ArgumentParser(
        description='Converts a Agisoft camera position file in Omega Phi Kappa format to a valid CSV')

    parser.add_argument("input", help="Filename of input file")
    parser.add_argument("output", help="Filename of output file")

    args = parser.parse_args()

    process(args.input, args.output)

if __name__ == "__main__":
    main(sys.argv[1:])
