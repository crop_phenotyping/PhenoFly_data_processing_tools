# PhenoFly data processing tools

&copy; Lukas Roth, Group of crop science, ETH Zurich

License: GPL-2 

## Content

The *PhenoFly data processing tool* repository hosts Python code related to **high-throughput field phenotyping** with unmanned aerial systems (UAS).
Methods are based on viewing geometries [1] and **multi-view images** [2].

Three major steps are required to preprocess flight campaigns:

1. [Image mask generation (Standalone Agisoft Script)](ImageProjectionAgisoft/README.md)
2. [Image segmentation with Random Forest](ActiveLearningSegmentation/README.md)
3. [Multi-view image generation](MultiViewImage/README.md)

After these steps, one of the following methods can be used to extract phenotyping traits:

- [Early growth trait extraction](EarlyGrowthTraitExtraction/README.md)

Additionally, usefull utils for other tasks can be found here:
- [Utils](Util/README.md)

## Deprecated content

- [Image mask generation (Ray tracing to DTM, deprecated)](ImageProjectionRayTracing/README.md)

---

[1]: Roth, Aasen, Walter, Liebisch (2018). Extracting leaf area index using viewing geometry effects—A new perspective on high-resolution unmanned aerial system photography, ISPRS Journal of Photogrammetry and Remote Sensing. https://doi.org/10.1016/j.isprsjprs.2018.04.012

[2]: Roth, Camenzind, Aasen, Kronenberg, Barendregt, Camp, Walter, Kirchgessner, Hund (2020). Repeated Multiview Imaging for Estimating Seedling Tiller Counts of Wheat Genotypes Using Drones. Plant Phenomics, 2020(3729715). https://doi.org/10.34133/2020/3729715