import geopandas as gpd
import pandas as pd
import numpy as np
import cv2
import matplotlib as mpl
mpl.use('Qt5Agg')
from matplotlib import pyplot as plt, gridspec
import imageio
from pathlib import Path
from matplotlib.path import Path as GeoPath
import sys
import shutil
from scipy.optimize import curve_fit
from sklearn.metrics import mean_squared_error
from math import sqrt

from Common import MaskRead


def P_p_from_CC_LC(view, CC, LC, c_1=0, c_2=0, azimuth_row = -0.890118):
    zenith, azimuth = view
    zenith = np.pi - zenith

    P = CC + (2 / np.pi) * np.tan(zenith) * (
            LC + c_1 * np.cos(2 * (azimuth - azimuth_row)) + c_2 * np.cos(4 * (azimuth - azimuth_row)))
    return (P)

def process_CC_AC_calc(path_processed_str, design_label):

    path_processed = Path(path_processed_str)
    path_GC_ACs = path_processed / 'GC_AC'

    # Get viewpoint dependant canopy coverage values
    df_canopy_coverage = pd.read_csv(str(path_GC_ACs / (design_label + "_" + "canopy_coverages.csv")))

    # Fit CC / LC function
    campaign_plot_groups = df_canopy_coverage.groupby(['plot_label'])

    plot_data = []
    for plot_label, plot_group in campaign_plot_groups:

        X = plot_group.canopy_coverage
        view = (plot_group['zenith_angle'], plot_group['azimuth_angle'])

        CC, LC = 0.5, 0.5

        bounds = (0, 1)
        popt, pcov = curve_fit(P_p_from_CC_LC, view, X, (CC, LC), bounds=bounds)

        X_pred = P_p_from_CC_LC(view, *popt)
        RMSE = sqrt(mean_squared_error(X, X_pred))

        CC, LC = popt

        plot_data.append({'plot_label': plot_label, 'CC': CC, 'LC': LC, 'CC_LC_RMSE': RMSE})

    df_plot_data = pd.DataFrame(plot_data)
    df_plot_data.to_csv(str(path_GC_ACs / (design_label + "_" + "CC_LC.csv") ), index=False)


def process_multiview_images_generation(path_processed_str, design_label, nx=50*20, ny=125, subplots=20, buffer=0, rmdest=False):
    print("Processing design", design_label)

    path_processed = Path(path_processed_str)
    path_segmented = path_processed / 'segmentation'
    path_masks = path_processed / 'image_masks'
    path_GC_ACs = path_processed / 'GC_AC'

    if rmdest:
        try:
            shutil.rmtree(path_GC_ACs, ignore_errors=True)
        except:
            print("Could not remove ", path_GC_ACs)

    path_GC_ACs.mkdir(exist_ok=True)


    # Read all masks
    gpd_masks = MaskRead.read_masks(path_masks, design_label)
    gpd_masks['geometry'] = gpd_masks.buffer(buffer)

    # Generate sample matrix and coords
    vx, vy = np.mgrid[0:nx, 0:ny]
    samplepoints_array = np.zeros((nx, ny))
    samplepoints_coords = np.stack((vx.flatten(), vy.flatten()), axis=1)
    samplepoints_normalized_coords = samplepoints_coords / (nx - 1, ny - 1)

    # Split plot label in group and subnumber
    if subplots:
        gpd_masks['plot_group'] = gpd_masks.plot_label.str[0:11]
        gpd_masks['plot_subnumber'] = np.int8(gpd_masks.plot_label.str[12:])

    # Dicts to store sample ground coverage / aerial coverage arrays in
    plot_coverages = []
    plot_GC_ACs = {}
    plot_image_count = {}
    plot_labels = np.unique(gpd_masks['plot_label'])
    for plot_label in plot_labels:
        plot_GC_ACs[plot_label] = samplepoints_array.copy().flatten()
        plot_image_count[plot_label] = 0

    # Process image by image
    image_groups = gpd_masks.groupby("image")
    for image, image_group in image_groups:
        print(f'Process {image}')

        # Load segmented image
        path_image = path_segmented / (image + '_predict.tif')
        if path_image.exists():
            image_segmented = imageio.imread(path_image)

            # Fill missing pixel positions with soil (soil_dominant) or plant pixel (plant dominant)
            image_soil_dominant, image_plant_dominant = image_segmented.copy(), image_segmented.copy()
            image_soil_dominant[image_soil_dominant ==   128] = 0
            image_plant_dominant[image_plant_dominant == 128] = 255

            # Erode with soil dominant mask (to erase small noisy plant pixels)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            mask_erase_soil_noise = cv2.morphologyEx(image_soil_dominant, cv2.MORPH_CLOSE, kernel)
            # Growth with plant dominant mask (to fill holes in segmented plants)
            mask_fill_plant_parts = cv2.morphologyEx(image_plant_dominant, cv2.MORPH_OPEN, kernel)
            # Apply masks
            image_segmented_filtered = image_segmented * mask_erase_soil_noise * mask_fill_plant_parts
            # Convert all resting 128 to plant pixels and normalize to [0, 1]
            image_segmented_filtered[image_segmented_filtered > 0] = 1

            # Process plots on image
            plot_label_groups = image_group.groupby("plot_label")
            for plot_label, plot_label_group in plot_label_groups:

                # Process different shapes
                for _, plot in plot_label_group.iterrows():
                    if plot['type'] == 'soil':
                        # Get postion of plot edges on image
                        plot_polygon = plot.at['geometry']
                        x, y = plot_polygon.exterior.coords.xy
                        plot_edges_image_coords = np.float32(np.stack([x, y], axis=1))

                        # Get affine transform matrix to transform from 0:1, 0:1 space to image coords
                        plot_edges_normalized_coords = np.float32([[0, 0], [0, 1], [1, 1], [1, 0]])
                        M_normalized_to_image_coords = cv2.getAffineTransform(plot_edges_normalized_coords[0:3, :], plot_edges_image_coords[0:3, :])

                        # Get position of plot raster points on image
                        samplepoints_image_coords = np.int16(
                            np.round(np.dot(np.c_[samplepoints_normalized_coords, np.ones(samplepoints_normalized_coords.shape[0])], M_normalized_to_image_coords.T)))

                        # Filter border pixels
                        border = 100
                        is_border = np.any(samplepoints_image_coords[:, 1] > image_segmented.shape[0]- 1 - border) or \
                            np.any(samplepoints_image_coords[:, 1] < 0 + border) or \
                            np.any(samplepoints_image_coords[:, 0] > image_segmented.shape[1] - 1 - border) or \
                            np.any(samplepoints_image_coords[:, 0] < 0 + border)

                        if not is_border:
                            # Sample oblique segmented image with samplepoints
                            sampled = image_segmented_filtered[samplepoints_image_coords[:, 1], samplepoints_image_coords[:, 0]]

                            ##DEBUG
                            '''
                            plt.figure()
                            plt.imshow(image_segmented_filtered)
                            plt.scatter(samplepoints_image_coords[:, 0], samplepoints_image_coords[:, 1])
                            plt.scatter(samplepoints_image_coords[50, 0], samplepoints_image_coords[50, 1])
                            plt.scatter(samplepoints_image_coords[100, 0], samplepoints_image_coords[100, 1])
                            plt.scatter(samplepoints_image_coords[200, 0], samplepoints_image_coords[200, 1])
                            plt.scatter(samplepoints_image_coords[600, 0], samplepoints_image_coords[600, 1])
                            
                            plt.scatter(samplepoints_image_coords[125:250, 0], samplepoints_image_coords[125:250, 1])
                            plt.scatter(samplepoints_image_coords[1000:1125, 0], samplepoints_image_coords[1000:1125, 1])
                            plt.show()

                            plt.figure()
                            sampled_img = sampled.reshape((50, 125))
                            plt.imshow(sampled_img.T)
                            plt.show()
                            '''

                            plot_GC_ACs[plot_label] += sampled
                            plot_image_count[plot_label] += 1

                            canopy_coverage = np.sum(sampled) / len(sampled)
                            plot['canopy_coverage'] = canopy_coverage
                            del(plot['geometry'])
                            plot_coverages.append(plot.to_dict())


    # Normalize GC_ACs with image counts
    for plot_label in plot_GC_ACs.keys():
        plot_GC_ACs[plot_label] /= plot_image_count[plot_label]

    # Save to disk
    try:
        path_GC_ACs.mkdir()
    except:
        pass


    if subplots:
        plot_groups = np.unique(gpd_masks['plot_group'])

        for plot_group in plot_groups:

            print(f"Summarizing {plot_group} with subplots")

            plots = []
            for i in range(20):
                plot_label = plot_group + "_" + str(i + 1)
                plot = plot_GC_ACs[plot_label].reshape(samplepoints_array.shape[0], samplepoints_array.shape[1])
                plot = np.uint8(plot * 255).T
                imageio.imsave(str(path_GC_ACs / (design_label + "_" + plot_label + ".tif")), plot)
                plots.append(plot)

            all_plots = np.concatenate(plots, axis=1)
            imageio.imsave(str(path_GC_ACs / (design_label + "_" + plot_group + ".tif")), all_plots)

    else:
        plot_labels = np.unique(gpd_masks['plot_label'])

        for plot_label in plot_labels:

            print(f"Summarizing {plot_label}")

            plot = plot_GC_ACs[plot_label].reshape(samplepoints_array.shape)
            plot = np.uint8(plot * 255).T
            imageio.imsave(str(path_GC_ACs / (design_label + "_" + plot_label + ".tif")), plot)

    # Save canopy coverages
    df_coverage = pd.DataFrame(plot_coverages)
    df_coverage.to_csv(str(path_GC_ACs / (design_label + "_" + "canopy_coverages.csv")), index=False)